const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

router.get('/', function(req, res) {
    res.send(FighterService.getAllFighters());
  });
  
  router.get('/:id', function(req, res) {
    const entity = FighterService.getFighterByField({ id: req.params.id });
    if (entity) {
      res.status(200).send(entity);
    } else {
      res.status(400).send({
        error: true,
        data: null,
        message: `Fighter with id=${req.params.id} doesn't exist`
      });
    }
  });
  
  router.post('/', createFighterValid, function(req, res) {
    const fighter = FighterService.createFighter(req.body);
    if(fighter) {
        res.status(200).send(fighter);
    } else {
        res.status(400).send({
            error: true,
            message: `Fighter can't be create`
        })
    }
  });
  
  router.put('/:id', updateFighterValid, function(req, res) {
    const updatedElement = FighterService.updateUserInfo(req.params.id, req.body);
    updatedElement
      ? res.send(updatedElement)
      : res.status(404).send({
          error: true,
          message: 'No such fighter'
        });
  });
  
  router.delete('/:id', function(req, res) {
    const deletedElement = FighterService.deleteFighter(req.params.id);
    if (deletedElement) {
      res.status(200).send(deletedElement); //return deleted element
    } else {
      res.status(404).send({
        error: true,
        message: `User with such id can't be found and deleted`
      });
    }
  });

module.exports = router;