const { Router } = require('express');
const UserService = require('../services/userService');
const {
  createUserValid,
  updateUserValid
} = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.get('/', function(req, res) {
  res.send(UserService.getAllUsers());
});

router.get('/:id', function(req, res) {
  const entity = UserService.getOne({ id: req.params.id });
  if (entity) {
    res.status(200).send(entity);
  } else {
    res.status(400).send({
      error: true,
      data: null,
      message: `User with id=${req.params.id} doesn't exist`
    });
  }
});

router.post('/', createUserValid, function(req, res) {
  res.status(200).send(UserService.createOne(req.body));
});

router.put('/:id', updateUserValid, function(req, res) {
  const updatedElement = UserService.updateUserInfo(req.params.id, req.body);
  updatedElement
    ? res.status(200).send(updatedElement)
    : res.status(404).send({
        error: true,
        message: 'No such user'
      });
});

router.delete('/:id', function(req, res) {
  const deletedElement = UserService.deleteOne(req.params.id);
  if (deletedElement) {
    res.status(200).send(deletedElement); //return deleted element
  } else {
    res.status(404).send({
      error: true,
      message: `User with such id can't be found and deleted`
    });
  }
});

module.exports = router;
