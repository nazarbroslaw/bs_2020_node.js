const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const cors = require('cors');
require('./config/db');

const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(function(error, req, res, next) {
  if (error instanceof SyntaxError) {
    return res.status(400).send({
      error: true,
      message: error.message
    });
  } else {
    next();
  }
});

const routes = require('./routes/index');
routes(app);

app.use('/', express.static(path.join(__dirname, 'client/build')));
const port = 3050;
app.listen(port, () => {});

exports.app = app;
