const { fighter } = require('../models/fighter');

const createFighterValid = (req, res, next) => {
    const { name, health, power, defense } = req.body;
    const fighterModelFields = Object.keys(fighter).filter(el => el !== 'id');
    for(let field of fighterModelFields) {
        if(!(field in req.body)) {
            return res.status(400).send({
                err: true,
                message: `${field} field is required`
            });
        }
    }

    const requiredFieldsExist = fighterModelFields.sort().toString() === Object.keys(req.body).sort().toString();
    if(!requiredFieldsExist) {
       return res.status(400).send({
            err: true,
            message: `Body must contains only firstName, lastName, email, phoneNumber, password fields`
        });
    }

    if(!(typeof(name) === 'string' && name.trim().length)) {
        return res.status(400).send({
            error: true,
            message: 'Name should be a string and contain at least one character'
        })
    }

    if(typeof(health) === 'number' && health > 100 || health < 1) {
        return res.status(400).send({
            error: true,
            message: 'Health must be between 1 and 100'
        })
    }

    if(!String(power).match(/^[1-9]+\.?\d*$/g)) {
        return res.status(400).send({
            error: true,
            message: 'Power must be positive number or valid number string'
        });
    }

    if(!String(defense).match(/^[0-9]\.\d*$|^([1-9]|10)$/g)) {
        return res.status(400).send({
            error: true,
            message: 'Defense must be between 1 - 10'
        });
    }

    next();
}

const updateFighterValid = (req, res, next) => {
    const { name, health, power, defense } = req.body;
    const fighterModelFields = Object.keys(fighter).filter(el => el !== 'id');
    for(let field of fighterModelFields) {
        if(!(field in req.body)) {
            return res.status(400).send({
                err: true,
                message: `${field} field is required`
            });
        }
    }

    const requiredFieldsExist = fighterModelFields.sort().toString() === Object.keys(req.body).sort().toString();
    if(!requiredFieldsExist) {
       return res.status(400).send({
            err: true,
            message: `Body must contains only firstName, lastName, email, phoneNumber, password fields`
        });
    }

    if(!(typeof(name) === 'string' && name.trim().length)) {
        return res.status(400).send({
            error: true,
            message: 'Name should be a string and contain at least one character'
        })
    }

    if(typeof(health) === 'number' && health > 100 || health < 1) {
        return res.status(400).send({
            error: true,
            message: 'Health must be between 1 and 100'
        })
    }

    if(!String(power).match(/^[1-9]+\.?\d*$/g)) {
        return res.status(400).send({
            error: true,
            message: 'Power must be positive number or valid number string'
        });
    }

    if(!String(defense).match(/^[0-9]\.\d*$|^([1-9]|10)$/g)) {
        return res.status(400).send({
            error: true,
            message: 'Defense must be between 1 - 10'
        });
    }
    next();
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;