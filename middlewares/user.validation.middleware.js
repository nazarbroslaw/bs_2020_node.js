const { user } = require('../models/user');
const UserService = require('../services/userService');

const Validator = require('validatorjs');


const createUserValid = (req, res, next) => {
    const { email, password, lastName, firstName, phoneNumber } = req.body;
    const userModelFields = Object.keys(user).filter(el => el !== 'id');
    for(let field of userModelFields) {
        if(!(field in req.body)) {
            res.status(400).send({
                err: true,
                message: `${field} field is required`
            });
            break;
        }
    }

    const requiredFieldsExist = userModelFields.sort().toString() === Object.keys(req.body).sort().toString();
    if(!requiredFieldsExist) {
        res.status(400).send({
            err: true,
            message: `Body must contains only firstName, lastName, email, phoneNumber, password fields`
        });
    }

    if(!String(req.body.password).match(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/g)) {
        res.status(400).send({
            err: true,
            message: `Password must contain at least one uppercase, lowercase, number. Min length 8`
        }); 
    }


    if(!(typeof(req.body.lastName) === "string" && req.body.lastName.length > 0)) {
        res.status(400).send({
            err: true,
            message: `Last name should be a string and contain at least one character`
        });
    }

    if(UserService.checkEmailValidity(email)) {
        console.log('ll',UserService.checkExistenceOfEmail(email))
        if(UserService.checkExistenceOfEmail(email)) {
            console.log('rrrr',UserService.checkExistenceOfEmail(email))
            return res.status(400).send({
                error: true,
                message: 'User with such email already exist'
            })
        }
    } else {
        res.status(400).send({
            error: true,
            message: 'Gmail email is appropriate only'
        })
    }
 
    if(!String(req.body.phoneNumber).match(/^\+380[1-9][0-9]{8}$/g)) {
        return res.status(400).send({
            err: true,
            message: `Phone format should be +380xxxxxxxxx`
        });
    } 

    next();
}

const updateUserValid = (req, res, next) => {
    const { id } = req.params; 
    // TODO: Implement validatior for user entity during update
    const { email, password, lastName, firstName, phoneNumber } = req.body;
    const userModelFields = Object.keys(user).filter(el => el !== 'id');
    for(let field of userModelFields) {
        if(!(field in req.body)) {
            res.status(400).send({
                err: true,
                message: `${field} field is required`
            });
            break;
        }
    }

    const requiredFieldsExist = userModelFields.sort().toString() === Object.keys(req.body).sort().toString();
    if(!requiredFieldsExist) {
        res.status(400).send({
            err: true,
            message: `Body must contains only firstName, lastName, email, phoneNumber, password fields`
        });
    }

    if(!String(req.body.password).match(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/g)) {
        res.status(400).send({
            err: true,
            message: `Password must contain at least one uppercase, lowercase, number. Min length 8`
        }); 
    }


    if(!(typeof(req.body.lastName) === "string" && req.body.lastName.length > 0)) {
        res.status(400).send({
            err: true,
            message: `Last name should be a string and contain at least one character`
        });
    }

    if(UserService.checkEmailValidity(email)) {
        console.log('ll',UserService.checkExistenceOfEmail(email))
        console.log('qf', UserService.getOne({email}));
        if(UserService.checkExistenceOfEmail(email) && UserService.getOne({email}).id !== id) {
            console.log('rrrr', UserService.checkExistenceOfEmail(email))
            return res.status(400).send({
                error: true,
                message: 'User with such email already exist'
            })
        }
    } else {
        res.status(400).send({
            error: true,
            message: 'Gmail email is appropriate only'
        })
    }

    if(!String(req.body.phoneNumber).match(/^\+380[1-9][0-9]{8}$/g)) {
        return res.status(400).send({
            err: true,
            message: `Phone format should be +380xxxxxxxxx`
        });
    }

    next();
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;