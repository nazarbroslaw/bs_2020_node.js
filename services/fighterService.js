const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {

    getAllFighters() {
        return FighterRepository.getAll();
    }

    getFighterByField(field) {
        return FighterRepository.getOne(field);
    }

    deleteFighter(id) {
        const deletedElement = FighterRepository.delete(id);
        return deletedElement.length ? deletedElement[0] : null;
    }

    updateUserInfo(id, data) {
        const updatedElement = FighterRepository.update(id, data);
        return updatedElement ? updatedElement : null;
    }

    createFighter(data) {
        return FighterRepository.create(data);
    }
}

module.exports = new FighterService();
