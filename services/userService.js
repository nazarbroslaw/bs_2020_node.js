const { UserRepository } = require('../repositories/userRepository');

class UserService {

  search(search) {
    const item = UserRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }

  getOne(field) {
    const item = this.search(field);
    return item;
  }

  createOne(data) {
    const newItem = UserRepository.create(data);
    return newItem;
  }

  getAllUsers() {
    return UserRepository.getAll();
  }

  deleteOne(id) {
    const deletedElement = UserRepository.delete(id);
    return deletedElement.length ? deletedElement[0] : null;
  }

  checkExistenceOfEmail(email) {
    return UserRepository.getOne({ email }) ? true : false;
  }

  checkEmailValidity(email) {
    return String(email).match(/^\w+@gmail.com$/g) ? true : false;
  }

  checkEmail(email) {
    if (String(email).match(/^\w+@gmail.com$/g)) {
      if (UserRepository.getOne({ email })) {
        return 'User with such email already exist';
      }
    } else {
      return 'Gmail email is appropriate only';
    }
    return null;
  }

  updateUserInfo(id, data) {
    const updatedElement = UserRepository.update(id, data);
    return updatedElement ? updatedElement : null;
  }
}

module.exports = new UserService();